using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Collectable : MonoBehaviour
{
    private static readonly HashSet<string> collected = new HashSet<string>();
    public static void ClearCollected()
    {
        collected.Clear();
    }

    private string GetId()
    {
        string sceneName = SceneManager.GetActiveScene().buildIndex.ToString();
        return sceneName + transform.position.ToString();
    }

    private void Awake()
    {
        if (collected.Contains(GetId()))
            Destroy(gameObject);
    }

    protected void Collect()
    {
        collected.Add(GetId());
    }
}
