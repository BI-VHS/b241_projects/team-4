using UnityEngine;

public class AddCrystal : Collectable
{
    private void Eat()
    {
        Collect();
        var reward = RewardPanel.Instance;
        reward.gameObject.SetActive(true);
        reward.SetRewardCrystal("M�te dal�� krystal!");
        reward.AddCallback(() => reward.ClearBoard());
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Eat();
    }
}
